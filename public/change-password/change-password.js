const inputFields = document.querySelectorAll('.input-field');

inputFields.forEach((item) => {
    item.addEventListener('click', (e) => {
        item.classList.toggle('active');
    })
});