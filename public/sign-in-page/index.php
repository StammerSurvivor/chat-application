<?php
require_once('../../app/init.php');


if (isset($_POST['sign-in'])) {
    $username = $_POST['username'];
    $password = $_POST['password'];
    
    $rememberMe = isset($_POST['remember-me']) ? true : false;

    if (Auth::signin($username, $password)) {
        if ($rememberMe) {
            $userToken = $token->createRememberMeToken(Auth::user()->id);
            // dd($userToken);
            setcookie("rememberme", $userToken->token, time() + Token::$REMEMBER_ME_EXPIRY_TIME_IN_SECS , '/');
            User::updateActive(Auth::user()->id , 1);
        }
        redirect(getURL('/pages/index.php'));
    } else {
        echo "Username/Password not match .....";
    }
}

if (secure($token, false)) {
    redirect('http://localhost:8000/pages/index.php');
}

?>


<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Login</title>
    <!-- GOOGLE FONTS -->
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Space+Grotesk:wght@300;400;500;600;700&display=swap" rel="stylesheet">
    <!-- CUSTOM CSS -->
    <link rel="stylesheet" href="./../css/reset.css">
    <link rel="stylesheet" href="./../css/general.css">
    <link rel="stylesheet" href="./../sign-in-page/main.css">
</head>

<body>
    <main>
        <div class="login-page p-2">
            <div class="login-image">
                <img src="./../Images/login.svg" alt="login">
            </div>
            <div class="login-details">
                <div class="hello-content text-center">
                    <h2>Hello Again!</h2>
                    <p>
                        Enter your login details to continue where you left off.
                        stay connected with your friends and family.
                        With our messaging and video calling features, you can easily communicate with your loved ones
                        from anywhere in the world
                    </p>
                </div>
                <form action="<?= $_SERVER['PHP_SELF'] ?>" method="POST">
                    <div class="username-input-section input-section">
                        <input type="text" id="username" class="username input-field" name="username" autocomplete="off">
                        <label for="username" class="input-section-label">username</label>
                        <span class="input-section-icon"><ion-icon name="at-outline"></ion-icon></span>
                    </div>
                    <div class="password-input-section input-section">
                        <input type="password" id="password" class="password input-field" name="password" autocomplete="off">
                        <label for="password" class="input-section-label">password</label>
                        <span class="input-section-icon">
                            <ion-icon class="lock-hide" name="lock-closed-outline"></ion-icon>
                        </span>
                    </div>
                    <div class="form-control">
                        <div class="remember-me-section">
                            <input type="checkbox" name="remember-me" id="remember-me" class="remember-me">
                            <label for="remember-me" class="remember-me-label">Remember me</label>
                        </div>
                        <div class="recovery-password">
                            <a href="./../recovery-password/index.php">recovery password</a>
                        </div>
                    </div>
                    <div class="submit-button my-1">
                        <div class="submit-form-button-div">
                            <input type="submit" value="Sign In" name="sign-in" class="submit-form-button">
                        </div>
                        <div class="other-sign-in-option">
                            <div class="other-sign-in-image">
                                <img src="./../Images/google.png" alt="google">
                            </div>
                            <div class="other-sign-in-head">sign in with google</div>
                        </div>
                    </div>
                </form>

                <p class="sign-up">don't have an account yet ? <span class="sign-up-link"><a href="http://localhost:9999/pages/sign-up-page/index.php">sign up</a></span></p>
            </div>
        </div>
    </main>

    <!-- ICONS -->
    <script type="module" src="https://unpkg.com/ionicons@7.1.0/dist/ionicons/ionicons.esm.js"></script>
    <script nomodule src="https://unpkg.com/ionicons@7.1.0/dist/ionicons/ionicons.js"></script>
    <!-- CUSTOM JS -->
    <script src="./login.js"></script>
</body>

</html>