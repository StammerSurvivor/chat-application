const inputFields = document.querySelectorAll('.input-field');

document.addEventListener('click', (e) => {
    inputFields.forEach(item => item.classList.toggle('active', item.contains(e.target)));
});
