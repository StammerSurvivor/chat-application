/******/ (() => { // webpackBootstrap
/******/ 	"use strict";
/******/ 	// The require scope
/******/ 	var __webpack_require__ = {};
/******/ 	
/************************************************************************/
/******/ 	/* webpack/runtime/define property getters */
/******/ 	(() => {
/******/ 		// define getter functions for harmony exports
/******/ 		__webpack_require__.d = (exports, definition) => {
/******/ 			for(var key in definition) {
/******/ 				if(__webpack_require__.o(definition, key) && !__webpack_require__.o(exports, key)) {
/******/ 					Object.defineProperty(exports, key, { enumerable: true, get: definition[key] });
/******/ 				}
/******/ 			}
/******/ 		};
/******/ 	})();
/******/ 	
/******/ 	/* webpack/runtime/hasOwnProperty shorthand */
/******/ 	(() => {
/******/ 		__webpack_require__.o = (obj, prop) => (Object.prototype.hasOwnProperty.call(obj, prop))
/******/ 	})();
/******/ 	
/******/ 	/* webpack/runtime/make namespace object */
/******/ 	(() => {
/******/ 		// define __esModule on exports
/******/ 		__webpack_require__.r = (exports) => {
/******/ 			if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 				Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 			}
/******/ 			Object.defineProperty(exports, '__esModule', { value: true });
/******/ 		};
/******/ 	})();
/******/ 	
/************************************************************************/
var __webpack_exports__ = {};
/*!********************************************!*\
  !*** ./resources/js/HttpRequestHandler.js ***!
  \********************************************/
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "HttpRequestHandler": () => (/* binding */ HttpRequestHandler)
/* harmony export */ });
function _typeof(obj) { "@babel/helpers - typeof"; return _typeof = "function" == typeof Symbol && "symbol" == typeof Symbol.iterator ? function (obj) { return typeof obj; } : function (obj) { return obj && "function" == typeof Symbol && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }, _typeof(obj); }
function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }
function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, _toPropertyKey(descriptor.key), descriptor); } }
function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); Object.defineProperty(Constructor, "prototype", { writable: false }); return Constructor; }
function _toPropertyKey(arg) { var key = _toPrimitive(arg, "string"); return _typeof(key) === "symbol" ? key : String(key); }
function _toPrimitive(input, hint) { if (_typeof(input) !== "object" || input === null) return input; var prim = input[Symbol.toPrimitive]; if (prim !== undefined) { var res = prim.call(input, hint || "default"); if (_typeof(res) !== "object") return res; throw new TypeError("@@toPrimitive must return a primitive value."); } return (hint === "string" ? String : Number)(input); }
var HttpRequestHandler = /*#__PURE__*/function () {
  function HttpRequestHandler() {
    _classCallCheck(this, HttpRequestHandler);
    this.xhr = new XMLHttpRequest();
    this.methods = {
      'GET': this.sendGetRequest,
      'POST': this.sendPostRequest,
      'PUT': this.sendPutRequest,
      'DELETE': this.sendDeleteRequest
    };
    this.headers = [];
  }
  _createClass(HttpRequestHandler, [{
    key: "setHeader",
    value: function setHeader(key, value) {
      this.headers.push({
        key: key,
        value: value
      });
    }
  }, {
    key: "setMethod",
    value: function setMethod(method) {
      this.method = method.toUpperCase();
    }
  }, {
    key: "setUrl",
    value: function setUrl(url) {
      this.url = url;
    }
  }, {
    key: "setBody",
    value: function setBody(body) {
      this.body = body;
    }
  }, {
    key: "setCSRFToken",
    value: function setCSRFToken(token) {
      this.token = token;
    }
  }, {
    key: "send",
    value: function send(callback) {
      var methodFunc = this.methods[this.method];
      if (!methodFunc) {
        throw new Error("Invalid HTTP method: ".concat(this.method));
      }
      methodFunc.call(this, callback);
    }
  }, {
    key: "sendGetRequest",
    value: function sendGetRequest(callback) {
      var _this = this;
      this.xhr.onload = function () {
        if (this.status == 200) {
          callback(JSON.parse(this.responseText));
        }
      };
      this.xhr.open('GET', this.url, true);
      this.headers.map(function (item) {
        _this.xhr.setRequestHeader(item.key, item.value);
      });
      this.xhr.send();
    }
  }, {
    key: "sendPostRequest",
    value: function sendPostRequest(callback) {
      var _this2 = this;
      this.xhr.onload = function () {
        if (this.status == 200) {
          callback(JSON.parse(this.responseText));
        }
      };
      this.xhr.open('POST', this.url, true);
      this.headers.map(function (item) {
        _this2.xhr.setRequestHeader(item.key, item.value);
      });
      this.xhr.send(JSON.stringify(this.body));
    }
  }, {
    key: "sendPutRequest",
    value: function sendPutRequest(callback) {
      var _this3 = this;
      this.xhr.onload = function () {
        if (this.status == 200) {
          callback(JSON.parse(this.responseText));
        }
      };
      this.xhr.open('PUT', this.url, true);
      this.headers.map(function (item) {
        _this3.xhr.setRequestHeader(item.key, item.value);
      });
      this.xhr.send(JSON.stringify(this.body));
    }
  }, {
    key: "sendDeleteRequest",
    value: function sendDeleteRequest(callback) {
      var _this4 = this;
      this.xhr.onload = function () {
        if (this.status == 200) {
          callback(JSON.parse(this.responseText));
        }
      };
      this.xhr.open('DELETE', this.url, true);
      this.headers.map(function (item) {
        _this4.xhr.setRequestHeader(item.key, item.value);
      });
      this.xhr.send();
    }
  }]);
  return HttpRequestHandler;
}();
/******/ })()
;