<?php 
    require_once('../../app/init.php');
    if(isset($_POST['sign-up'])) {
        $tempFileName = $_FILES['pic']['name'];       
        
        $tempFilePath = $_FILES['pic']['tmp_name'];

        if ($tempFileName === '') { 
            $tempFileName = 'default.png';
        }

        $fileNameAsArray = explode('.', $tempFileName);

        $ext = end($fileNameAsArray);
        

        // dd($tempFileName);
        // dd(generateUniqueId());
        
        $unique_id = generateUniqueId();
        $username = $_POST['username'];
        $email = $_POST['email'];
        $password = $_POST['password'];

        if($tempFileName === 'default.png') {
            $image_name = "default.png";             
        } else {
            $image_name = "$unique_id.$ext";            
            move_uploaded_file($tempFilePath, "./../Images/users/$image_name");
        }

        // $validator->check($_POST, [
        //     'email' => [
        //         'required' => true,
        //         'email' => true,
        //         'minlength' => 5,
        //         'maxlength' => 255,
        //         'unique' => "users"
        //     ],
        //     'username' => [
        //         'required' => true,
        //         'maxlength' => 20,
        //         'unique' => "users"
        //     ],
        //     'password' => [
        //         'required' => true,
        //         'minlength' => 0,
        //         'password' => true
        //     ]
        // ]);
        
        User::create([
            'unique_id' => $unique_id,
            'username' => $username,
            'email' => $email,
            'password' => $password,
            'image_name' => $image_name,
        ]);
        redirect('http://localhost:9999/pages/sign-in-page/index.php');
    }

?>



<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Sign UP</title>
    <!-- GOOGLE FONTS -->
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Space+Grotesk:wght@300;400;500;600;700&display=swap"
        rel="stylesheet">

    <!-- CUSTOM CSS -->
    <link rel="stylesheet" href="./../css/reset.css">
    <link rel="stylesheet" href="./../css/general.css">
    <link rel="stylesheet" href="./../sign-up-page/main.css">
</head>

<body>
    <main>
        <div class="sign-up-page p-2">
            <div class="sign-up-details">
                <div class="register-content">
                    <h1>Join our Talkster now!</h1>
                    <p>
                        Don't have an account yet? Sign up now to join the millions of users who are already benefiting
                        from our platform.
                    </p>
                </div>
                <form action="" method="post" enctype="multipart/form-data">
                    <div class="profile-photo">
                        <div class="profile-pic">
                            <img src="./../Images/default-2.png" alt="user-img">
                        </div>
                        <button class="user-profile-img-button">
                            <span>
                                <ion-icon name="camera-outline"></ion-icon>
                            </span>
                            Upload Photo
                            <input type="file" name="pic" id="pic" data-error=".pic_error">
                        </button>
                    </div>
                    <div class="username-input-section input-section">
                        <input type="text" id="username" class="username input-field" name="username"
                            autocomplete="on">
                        <label for="username" class="input-section-label">username</label>
                        <span class="input-section-icon"><ion-icon name="person-outline"></ion-icon></span>
                    </div>
                    <div class="email-input-section input-section">
                        <input type="text" id="email" class="email input-field" name="email" autocomplete="on">
                        <label for="email" class="input-section-label">email</label>
                        <span class="input-section-icon"><ion-icon name="at-outline"></ion-icon></span>
                    </div>
                    <div class="password-input-section input-section">
                        <input type="password" id="password" class="password input-field" name="password"
                            autocomplete="on">
                        <label for="password" class="input-section-label">password</label>
                        <span class="input-section-icon">
                            <ion-icon class="lock-hide" name="lock-closed-outline"></ion-icon>
                        </span>
                    </div>
                    <div class="confirm-password-input-section input-section">
                        <input type="password" id="confirm-password" class="confirm-password input-field"
                            name="password" autocomplete="on">
                        <label for="confirm-password" class="input-section-label">confirm password</label>
                        <span class="input-section-icon">
                            <ion-icon class="lock-hide" name="lock-closed-outline"></ion-icon>
                        </span>
                    </div>
                    <div class="submit-form-button-div">
                        <input type="submit" value="Sign-Up" name="sign-up" class="submit-form-button">
                    </div>
                </form>

                <p class="login text-right">already have an account ? <span class="login-link"><a href="http://localhost:9999/pages/sign-in-page/index.php">sign
                            in</a></span></p>

            </div>
            <div class="sign-up-image">
                <img src="./../Images/sign-up.svg" alt="">
            </div>
        </div>
    </main>
    <!-- ICONS -->
    <script type="module" src="https://unpkg.com/ionicons@7.1.0/dist/ionicons/ionicons.esm.js"></script>
    <script nomodule src="https://unpkg.com/ionicons@7.1.0/dist/ionicons/ionicons.js"></script>
    <!-- CUSTOM JS -->
    <script src="./../sign-up-page/signup.js"></script>
</body>

</html>