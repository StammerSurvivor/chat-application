const image_input = document.querySelector("#pic");
const profile_pic = document.querySelector(".profile-pic")
const inputFields = document.querySelectorAll('.input-field');

image_input.addEventListener("change", function () {
    changeImage(this);
});

function changeImage(input) {
    var reader;

    if (input.files && input.files[0]) {
        reader = new FileReader();
        reader.onload = function (e) {
            profile_pic.children[0].setAttribute('src', e.target.result);
        }

        reader.readAsDataURL(input.files[0]);
    }
}


inputFields.forEach((item) => {
    item.addEventListener('click', (e) => {
        item.classList.add('active');
    })
});

inputFields.forEach((item) => {
    item.addEventListener('focus', (e) => {
        item.classList.toggle('active');
    })
});

