const otpBox = document.querySelectorAll('.otp-box');
const otpSection = document.querySelectorAll('.otp-section');
otpBox.forEach((element, index) => {
    element.children[0].addEventListener('keyup', (e) => {
        if (e.key >= 0 && e.key <= 9) {
            if (index === otpSection[0].childElementCount - 1) {
                element.parentElement.children[index].children[0].blur();
            } else {
                element.parentElement.children[index + 1].children[0].focus();
            }
        }
    });
});