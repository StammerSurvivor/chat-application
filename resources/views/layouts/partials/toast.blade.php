<!-- TOAST -->
<div class="toast p-1 {{ $active }}" style="--toast-color : {{ $toast_color }}">
    <div class="toast-content">
        <span class="toast-icon">
            <ion-icon name="{{ $icon_name }}"></ion-icon>
        </span>
        <div class="toast-message">
            <p>{{ $title }} : </p>
            <p>{{ $message }}</p>
        </div>
        <span class="toast-close-button">
            <ion-icon name="close-outline"></ion-icon>
        </span>
    </div>
    <div class="progress-bar" ></div>
</div>
<!-- /TOAST -->
