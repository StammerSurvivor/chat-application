<div class="chat-navigation-bar chats-navigation-bar prevent-select">
    <!-- HEAD -->
    <div class="chat-navigation-head">
        <h1>Chats</h1>
        <div class="chat-head-option d-flex">
        <p class="head-options head-option-1 new-chat-option" id="new-chat-group">
                <ion-icon name="create-outline"></ion-icon>
            </p>
            <p class="head-options head-option-2 filter-option">
                <ion-icon name="ellipsis-vertical-outline"></ion-icon>
            </p>
        </div>
    </div>
    <div class="chat-lists mt-1" data-check="0">
        @foreach ($records as $key => $record)
            <div class='chat-list chat-list-{{ $key + 1 }}'>
                <div class='chat-list-image'>
                    <img src="{{ asset('/Images/users/' . $record->image_name) }}" alt='chat-list-logo' loading="lazy">
                    <input type='hidden' user-id={{ $record->unique_id }} name='user-id' />
                    @if ($record->active)
                        <input type='text' value="Online" name='user-status' class="user-status" />
                    @else
                        <input type='text' value="Offline" name='user-status' class="user-status" />
                    @endif
                </div>
                <div class='chat-list-title'>
                    <div class='chat-list-content'>
                        <p class='chat-list-title-name' style='text-transform: capitalize;'>{{ $record->name }}</p>
                        <p class='chat-list-date'>{{ str_replace('-', '/', $record->created_at->format('d-m-Y')) }}
                        </p>
                    </div>
                    <div class='chat-list-message'>{{ $record->message[0] }}</div>
                </div>
            </div>
        @endforeach
    </div>
</div>
