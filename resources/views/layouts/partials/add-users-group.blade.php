<!-- ALL USERS MODAL -->
<div class="add-group-users">
    <div class="add-section">
        <form action="{{ route('create-chat-group') }}" method="POST" enctype="multipart/form-data">
            @csrf
            <div class="header-section">
                <h1>New Group</h1>
                <p class="add-group-contact-cancel">
                    <ion-icon name="close-outline"></ion-icon>
                </p>
            </div>
            <div class="group-section-photo-name">
                <div class="group-profile-photo">
                    <div class="group-pic">
                        <img src="{{ asset('Images/Group/default.png') }}" alt="user-group-img">
                        <input type="file" name="pic" id="pic" data-error=".pic_error">
                    </div>
                </div>
                <div class="group-subject">
                    <input type="text" name="group-section-name" id="group-section-name"
                        placeholder="Enter a Subject">
                </div>
            </div>
            <div class="search-section">
                <input type="text" placeholder="Search">
            </div>
            <div class="contacts-section">
                <h4 class="contacts-section-heading">All Contacts</h4>
                <div class="contacts-section-list">
                </div>
                <div class="create-button">
                    <input type="submit" value="Create Group">
                </div>
            </div>
        </form>
    </div>
</div>
