<header class="prevent-select">
    <div class="page-logo">
        <img src="{{ asset('Images/chat_logo.png') }}" alt="chat_logo">
    </div>
    <h1>Talkster</h1>
</header>