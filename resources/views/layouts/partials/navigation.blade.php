<div class="navigation-bar">
    <div class="navigation-1">
        <p id="chat-icon" class="icons chat-icon" data-description="chat">
            <ion-icon name="chatbubble-ellipses-outline"></ion-icon>
        </p>
        <p id="call-icon" class="icons call-icon" data-description="calls">
            <ion-icon name="call-outline"></ion-icon>
        </p>
        <p id="status-icon" class="icons status-icon" data-description="status">
            <ion-icon name="aperture-outline"></ion-icon>
        </p>
        <p id="group-icon" class="icons status-icon" data-description="Groups">
            <ion-icon name="people-outline"></ion-icon>
        </p>
    </div>
    <div class="navigation-2">
        <p class="icons icon-settings" data-description="settings">
            <ion-icon name="settings-outline"></ion-icon>
        </p>
        <p class="profile-with-icon" data-description="profile">
            <img src="{{ asset('Images/users/' . Auth::user()->image_name) }}" alt="user-image">
        </p>
    </div>
</div>
