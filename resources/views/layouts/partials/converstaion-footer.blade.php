<div class="conversation-footer">
    <div class="conversation-footer-content">
        <div class="d-flex">
            <div class="conversation-options emojies-option">
                <ion-icon name="happy-outline"></ion-icon>
            </div>
            <div class="conversation-options file-option">
                <ion-icon name="document-attach-outline"></ion-icon>
            </div>
        </div>
        <form action="" method="post" class="d-flex" id="message-form">
            <input type="hidden" class="hidden-receiver-id" name="receiver_id" value="">
            <input type="hidden" class="hidden-group-id" name="group_id" value="">
            <input type="hidden" class="hidden-sender-id" name="sender_id" value="{{ Auth::user()->unique_id }}">
            <div class="conversation-input"><input type="text" class="conversation-input-field"
                    placeholder="Type a Message" name="message" autocomplete="off"></div>
            <button type="submit" name="message-submit" class="submit-button conversation-options">
                <ion-icon name="mic-outline"></ion-icon>
            </button>
        </form>
        <!-- <div class="conversation-footer-right-option conversation-options"><ion-icon name="mic-outline"></ion-icon></div> -->
    </div>
</div>
