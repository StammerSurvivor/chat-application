<div class="welcome-page-chat prevent-select">
    <div class="welcome-page-chat-image">
        <img src="{{ asset('Images/chat_logo.png') }}" alt="chat_logo">
    </div>
    <h1>talkster for windows</h1>
    <p>send and receive messages without keeping your phone online.</p>
    <p>use talkster on up to 4 linked devices and 1 phone at the same time</p>
</div>