<div class="conversation-header">
    <div class="conversation-header-detail">
        <div class="conversation-header-profile">
            <img src="{{ asset('Images/chat-list-logo.png') }}" alt="usr-img">
        </div>
        <div class="d-flex" style="flex-direction: column">
            <h1 class="conversation-header-user-name"></h1>
            <p class="conversation-header-user-status user-offline "></p>
        </div>
    </div>
    <div class="calls-options">
        <div class="custom-option phone-call-option">
            <ion-icon name="call-outline"></ion-icon>
        </div>
        <div class="custom-option video-call-option">
            <ion-icon name="videocam-outline"></ion-icon>
        </div>
        <div class="custom-option search-option">
            <ion-icon name="search-outline"></ion-icon>
        </div>
    </div>
</div>
