<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>@yield('title')</title>
    <!-- GOOGLE FONTS -->
    <link rel="preconnect" href={{ asset('https://fonts.googleapis.com') }}>
    <link rel="preconnect" href={{ asset("https://fonts.gstatic.com") }} crossorigin>
    <link href={{ asset('https://fonts.googleapis.com/css2?family=Space+Grotesk:wght@300;400;500;600;700&display=swap') }} rel="stylesheet">

    <!-- CUSTOM CSS -->
    <link rel="stylesheet" href="{{ asset('css/reset.css') }}">
    <link rel="stylesheet" href="{{ asset('css/general.css') }}">
    <link rel="stylesheet" href=@yield('style')>
</head>
<body>   
    
    @yield('page-content')

    <!-- ICONS -->
    <script type="module" src="{{ asset('https://unpkg.com/ionicons@7.1.0/dist/ionicons/ionicons.esm.js') }}"></script>
    <script nomodule src="{{ asset('https://unpkg.com/ionicons@7.1.0/dist/ionicons/ionicons.js') }}"></script>
    <!-- CUSTOM JS -->
    <script src=@yield('script')></script>
</body>
</html>