@extends('layouts.app')

{{-- TITLE --}}
@section('name', 'Change Password')

{{-- STYLE --}}
@section('style', asset('change-password/main.css'))

{{-- PAGE CONTENT --}}
@section('page-content')
    <<main>
        <div class="change-password p-2">
            <div class="change-password-image">
                <img src="{{ asset('Images/forgot-password.svg') }}" alt="forgot-password.svg">
            </div>
            <div class="change-password-details">
                <div class="change-password-content text-center">
                    <h2>Change Password!</h2>
                    <p>your password must be at least six characters and should include a combination of
                        numbers,letters, and special characters (!$@%)</p>
                </div>
                <form action="" method="POST">
                    <div class="password-input-section input-section">
                        <input type="password" id="old-password" class="old-password input-field" name="password"
                            autocomplete="off">
                        <label for="old-password" class="input-section-label">password</label>
                        <span class="input-section-icon">
                            <ion-icon class="lock-hide" name="lock-closed-outline"></ion-icon>
                        </span>
                    </div>
                    <div class="new-password-input-section input-section">
                        <input type="password" id="new-password" class="new-password input-field" name="password"
                            autocomplete="off">
                        <label for="new-password" class="input-section-label">new password</label>
                        <span class="input-section-icon">
                            <ion-icon class="lock-hide" name="lock-closed-outline"></ion-icon>
                        </span>
                    </div>
                    <div class="confirm-new-password-input-section input-section">
                        <input type="password" id="confirm-new-password" class="confirm-new-password input-field"
                            name="password" autocomplete="off">
                        <label for="confirm-new-password" class="input-section-label">confirm new password</label>
                        <span class="input-section-icon">
                            <ion-icon class="lock-hide" name="lock-closed-outline"></ion-icon>
                        </span>
                    </div>

                    <div class="submit-button my-1"></div>
                </form>
                </p>
            </div>
        </div>
    </main>
@endsection

{{-- SCRIPT --}}
@section('script', asset('change-password/change-password.js'))
