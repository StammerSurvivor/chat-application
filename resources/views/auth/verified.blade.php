@extends('layouts.app')


{{-- TITLE --}}
@section('name', 'Verified Email')

{{-- STYLE --}}
@section('style', asset('verified-email/main.css'))

{{-- PAGE CONTENT --}}
@section('page-content')
    <main>
        <div class="verified-email-page p-2">
            <div class="verified-email-img">
                <img src="{{ asset('Images/check.png') }}" alt="check.png">
            </div>
            <h1>Your are now verified</h1>
            <p>yahoo! you have successfully verified your account</p>
            <button><a href={{ route('auth.user.dashboard') }}>Go To Home Page</a></button>
        </div>
    </main>
@endsection

{{-- SCRIPT --}}
@section('script', '')
