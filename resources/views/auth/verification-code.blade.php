@extends('layouts.app')

{{-- TITLE --}}
@section('name', 'Verification')

{{-- STYLE --}}
@section('style', asset('verification-code/main.css'))

{{-- PAGE CONTENT --}}
@section('page-content')
    <main>
        <div class="otp-page p-2">
            <div class="otp-image">
                <img src="{{ asset('Images/otp.svg') }}" alt="otp.svg">
            </div>
            <div class="otp-details">
                <h2 class="otp-head">verification code</h2>
                <p class="otp-description">we have send the code verification to your email address</p>
                <p class="given-email">gautam@gmail.com</p>
                <div class="otp-section mt-1">
                    <div class="otp-box otp-box-1">
                        <input type="text">
                    </div>
                    <div class="otp-box otp-box-2">
                        <input type="text">
                    </div>
                    <div class="otp-box otp-box-3">
                        <input type="text">
                    </div>
                    <div class="otp-box otp-box-4">
                        <input type="text">
                    </div>
                    <div class="otp-box otp-box-5">
                        <input type="text">
                    </div>
                    <div class="otp-box otp-box-6">
                        <input type="text">
                    </div>
                </div>
                <button class="otp-submit-button">Submit</button>
            </div>
        </div>
    </main>
@endsection

{{-- SCRIPT --}}
@section('script', asset('verification-code/verification-code.js'))
