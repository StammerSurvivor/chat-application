@extends('layouts.app');

{{-- TITLE --}}
@section('name', 'Verify Email')

{{-- STYLE --}}
@section('style', asset('verify-mail/main.css'));

{{-- PAGE CONTENT --}}
@section('page-content')
    <main>
        <div class="verify-email-page p-2">
            <div class="verify-email-image">
                <img src="{{ asset('Images/verify.svg') }}" alt="verify.svg">
            </div>
            <h2>verify your email address</h2>
            <p class="verify-email-description text-center">you've entered <span>gautam@gmail.com</span> as the email address for your account.
                <br> please verify the email address by clicking the button below
            </p>
            <input type="submit" class="verify-button" value="Verify Your Email" />
        </div>
    </main>
@endsection

{{-- SCRIPT --}}
@section('script', asset('verify-mail/verify-main.js'))
