@extends('layouts.app')

{{-- TITLE --}}
@section('name', 'Recovery Password')

{{-- STYLE --}}
@section('style', asset('recovery-password/main.css'))

{{-- PAGE CONTENT --}}
@section('page-content')
<main>
    <div class="recovery-password p-2">
        <div class="recovery-password-image">
            <img src="{{ asset('Images/change-password.svg') }}" alt="change-password.svg">
        </div>
        <div class="recovery-password-page">
            <h1>Recovery Password</h1>
            <form action="" method="post">
                <div class="password-input-section input-section">
                    <input type="password" id="new-password" class="new-password input-field" name="password"
                        autocomplete="off">
                    <label for="new-password" class="input-section-label">enter email</label>
                    <span class="input-section-icon">
                        <ion-icon class="lock-hide" name="at-outline"></ion-icon>
                    </span>
                </div>

                <input type="submit" value="change-password" class="change-password-button">
            </form>
        </div>
    </div>
</main>
@endsection

{{-- SCRIPT --}}
@section('script', asset('recovery-password/recovery-password.js'))
