@extends('layouts.app')

{{-- TITLE --}}
@section('name', 'Login')

{{-- STYLE --}}
@section('style', asset('sign-in-page/main.css'))

{{-- PAGE CONTENT --}}
@section('page-content')
    <main>
        <div class="login-page p-2">
            <div class="login-image">
                <img src="{{ asset('Images/login.svg') }}" alt="login.svg">
            </div>
            <div class="login-details">
                <div class="hello-content text-center">
                    <h2>Hello Again!</h2>
                    <p>
                        Enter your login details to continue where you left off.
                        stay connected with your friends and family.
                        With our messaging and video calling features, you can easily communicate with your loved ones
                        from anywhere in the world
                    </p>
                </div>
                <form action="{{ route('auth.login.check') }}" method="POST">

                    @if (Session::get('Fail'))
                        @include('layouts.partials.toast', [
                            'active' => 'active',
                            'title' => 'Invalid',
                            'message' => 'Email or Password,please try again!',
                            'icon_name' => 'alert-circle-outline',
                            'toast_color' => 'red',
                        ])
                    @endif

                    @if (Session::get('Session Expires'))
                        @include('layouts.partials.toast', [
                            'active' => 'active',
                            'title' => 'Session Expired',
                            'message' => 'Your Session Has been expired please login again!',
                            'icon_name' => 'alert-circle-outline',
                            'toast_color' => '#43A6C6',
                        ])
                    @endif

                    @if (Session::get('Unauthorized'))
                        @include('layouts.partials.toast', [
                            'active' => 'active',
                            'title' => 'Unauthorized',
                            'message' => 'Acess Denied',
                            'icon_name' => 'skull-outline',
                            'toast_color' => 'red',
                        ])
                    @endif

                    @csrf
                    <div class="email-input-section input-section">
                        <input type="text" id="email" class="email input-field" name="email" autocomplete="on"
                            value="{{ old('email') }}">
                        <label for="email" class="input-section-label">email</label>
                        <span class="input-section-icon">
                            <ion-icon name="at-outline"></ion-icon>
                        </span>
                        <span class="text-danger">
                            @error('email')
                                {{ $message }}
                            @enderror
                        </span>
                    </div>
                    <div class="password-input-section input-section">
                        <input type="password" id="password" class="password input-field" name="password"
                            autocomplete="off">
                        <label for="password" class="input-section-label">password</label>
                        <span class="input-section-icon">
                            <ion-icon class="lock-hide" name="lock-closed-outline"></ion-icon>
                        </span>
                    </div>

                    <div class="form-control">
                        <div class="remember-me-section">
                            <input type="checkbox" name="remember_me" id="remember-me" class="remember-me">
                            <label for="remember-me" class="remember-me-label">Remember me</label>
                        </div>
                        <div class="recovery-password">
                            <a href="{{ route('auth.recovery.password') }}">recovery password</a>
                        </div>
                    </div>
                    <div class="submit-button my-1">
                        <div class="submit-form-button-div">
                            <input type="submit" value="Sign In" name="sign-in" class="submit-form-button">
                        </div>
                        <div class="other-sign-in-option">
                            <div class="other-sign-in-image">
                                <img src="{{ asset('Images/google.png') }}" alt="google">
                            </div>
                            <div class="other-sign-in-head">sign in with google</div>
                        </div>
                    </div>
                </form>

                <p class="sign-up">don't have an account yet ? <span class="sign-up-link"><a
                            href="{{ route('auth.register') }}">sign up</a></span></p>
            </div>
        </div>
    </main>
@endsection

{{-- SCRIPT --}}
@section('script', asset('sign-in-page/login.js'))
