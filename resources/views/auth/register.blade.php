@extends('layouts.app')

{{-- TITLE --}}
@section('name', 'Sign Up')

{{-- STYLE --}}
@section('style', asset('sign-up-page/main.css'))

{{-- PAGE CONTENT --}}
@section('page-content')
    <main>
        <div class="sign-up-page p-2">
            <div class="sign-up-details">
                <div class="register-content">
                    <h1>Join our Talkster now!</h1>
                    <p>
                        Don't have an account yet? Sign up now to join the millions of users who are already benefiting
                        from our platform.
                    </p>
                </div>
                <form action="{{ route('auth.register.store') }}" method="post" enctype="multipart/form-data">
                    @if (Session::get('Success'))
                        @include('layouts.partials.toast', [
                            'active' => 'active',
                            'title' => 'Success',
                            'message' => "You're Successfully register, Now Please Login!",
                            'icon_name' => 'checkmark-outline',
                            'toast_color' => '#0abf30',
                        ])
                    @endif

                    @if (Session::get('Fail'))
                        @include('layouts.partials.toast', [
                            'active' => 'active',
                            'title' => 'Oops',
                            'message' => 'Something went wrong , please try again later!',
                            'icon_name' => 'cloud-offline-outline',
                            'toast_color' => 'red',
                        ])
                    @endif

                    @csrf
                    <div class="profile-photo">
                        <div class="profile-pic">
                            <img src="{{ asset('Images/default-2.png') }}" alt="user-img">
                        </div>
                        <button class="user-profile-img-button">
                            <span>
                                <ion-icon name="camera-outline"></ion-icon>
                            </span>
                            Upload Photo
                            <input type="file" name="pic" id="pic" data-error=".pic_error">
                        </button>
                    </div>
                    <div class="username-input-section input-section">
                        <input type="text" id="username" class="username input-field" name="name" autofocus="on" autocomplete="on" value="{{ old('name') }}">
                        <label for="username" class="input-section-label">username</label>
                        <span class="input-section-icon">
                            <ion-icon name="person-outline"></ion-icon>
                        </span>
                        <span class="text-danger">
                            @error('name')
                                {{ $message }}
                            @enderror
                        </span>
                    </div>
                    <div class="email-input-section input-section">
                        <input type="text" id="email" class="email input-field" name="email" autofocus="on" autocomplete="on" value="{{ old('email') }}">
                        <label for="email" class="input-section-label">email</label>
                        <span class="input-section-icon">
                            <ion-icon name="at-outline"></ion-icon>
                        </span>                        
                        <span class="text-danger">
                            @error('email')
                                {{ $message }}
                            @enderror
                        </span>
                    </div>
                    <div class="password-input-section input-section">
                        <input type="password" id="password" class="password input-field" name="password"
                            autocomplete="on">
                        <label for="password" class="input-section-label">password</label>
                        <span class="input-section-icon">
                            <ion-icon class="lock-hide" name="lock-closed-outline"></ion-icon>
                        </span>
                        <span class="text-danger">
                            @error('password')
                                {{ $message }}
                            @enderror
                        </span>
                    </div>
                    <div class="confirm-password-input-section input-section">
                        <input type="password" id="confirm-password" class="confirm-password input-field" name="password_confirmation"
                            autocomplete="on">
                        <label for="confirm-password" class="input-section-label">confirm password</label>
                        <span class="input-section-icon">
                            <ion-icon class="lock-hide" name="lock-closed-outline"></ion-icon>
                        </span>
                        <span class="text-danger">
                            @error('password_confirmation')
                                {{ $message }}
                            @enderror
                        </span>
                    </div>
                    <div class="submit-form-button-div">
                        <input type="submit" value="Sign-Up" name="sign-up" class="submit-form-button">
                    </div>
                </form>

                <p class="login text-right">already have an account ? <span class="login-link"><a
                            href="{{ route('auth.login') }}">sign
                            in</a></span></p>

            </div>
            <div class="sign-up-image">
                <img src="{{ asset('Images/sign-up.svg') }}" alt="sign-up.svg">
            </div>
        </div>
    </main>



@endsection

{{-- SCRIPT --}}
@section('script', asset('sign-up-page/signup.js'))
