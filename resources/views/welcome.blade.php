@php
    use Illuminate\Support\Facades\Auth;
@endphp


<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    
    <title>Talkster | Chat App</title>
    <!-- GOOGLE FONTS -->
    <link rel="preconnect" href={{ asset('https://fonts.googleapis.com') }}>
    <link rel="preconnect" href={{ asset('https://fonts.gstatic.com') }} crossorigin>
    <link
        href={{ asset('https://fonts.googleapis.com/css2?family=Space+Grotesk:wght@300;400;500;600;700&display=swap') }}
        rel="stylesheet">

    <!-- CUSTOM CSS -->
    <link rel="stylesheet" href="{{ asset('css/reset.css') }}">
    <link rel="stylesheet" href="{{ asset('css/general.css') }}">
    <link rel="stylesheet" href="{{ asset('css/main.css') }}">
    <link rel="stylesheet" href="{{ asset('css/responsive.css') }}">
    <!-- ICONS -->
    <script type="module" src="{{ asset('https://unpkg.com/ionicons@7.1.0/dist/ionicons/ionicons.esm.js') }}"></script>
    <script nomodule src="{{ asset('https://unpkg.com/ionicons@7.1.0/dist/ionicons/ionicons.js') }}"></script>
</head>

<body>

    {{-- {{ $records }} --}}
    @include('layouts.partials._header')
    <main>
        @include('layouts.partials.navigation')

        @include('layouts.partials.chats-list')
        @include('layouts.partials.group-list')
        
        <div class="chat">
            @include('layouts.partials.welcome-message')
            @include('layouts.partials.user-chat')
        </div>
    </main>


    @include('layouts.partials._overlay')
    @include('layouts.partials.add-users-group')
    
    
    <!-- CUSTOM JS -->
    <script>var sender_id = @json(Auth::user()->unique_id);</script>
    <script src="{{ asset('js/app.js') }}"></script>
</body>

</html>
