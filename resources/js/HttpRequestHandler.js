export class HttpRequestHandler {
  constructor() {
    this.xhr = new XMLHttpRequest();
    this.methods = {
      'GET': this.sendGetRequest,
      'POST': this.sendPostRequest,
      'PUT': this.sendPutRequest,
      'DELETE': this.sendDeleteRequest
    };
    this.headers = [];
  }

  setHeader(key, value) {
    this.headers.push({ key : key , value : value });
  }

  setMethod(method) {
    this.method = method.toUpperCase();
  }

  setUrl(url) {
    this.url = url;
  }

  setBody(body) {
    this.body = body;
  }

  setCSRFToken(token) {
    this.token = token;
  }

  send(callback) {
    const methodFunc = this.methods[this.method];
    if (!methodFunc) {
      throw new Error(`Invalid HTTP method: ${this.method}`);
    }
    methodFunc.call(this, callback);
  }

  sendGetRequest(callback) {
    this.xhr.onload = function () {
      if (this.status == 200) {
        callback(JSON.parse(this.responseText));
      }
    }
    this.xhr.open('GET', this.url, true);
    this.headers.map((item) => {
      this.xhr.setRequestHeader(item.key, item.value);
    });

    this.xhr.send();
  }

  sendPostRequest(callback) {
    this.xhr.onload = function () {
      if (this.status == 200) {
        callback(JSON.parse(this.responseText));
      }
    }
    this.xhr.open('POST', this.url, true);

    this.headers.map((item) => {
      this.xhr.setRequestHeader(item.key, item.value);
    });

    this.xhr.send(JSON.stringify(this.body));
  }

  sendPutRequest(callback) {
    this.xhr.onload = function () {
      if (this.status == 200) {
        callback(JSON.parse(this.responseText));
      }
    }
    this.xhr.open('PUT', this.url, true);

    this.headers.map((item) => {
      this.xhr.setRequestHeader(item.key, item.value);
    });

    this.xhr.send(JSON.stringify(this.body));
  }

  sendDeleteRequest(callback) {
    this.xhr.onload = function () {
      if (this.status == 200) {
        callback(JSON.parse(this.responseText));
      }
    }
    this.xhr.open('DELETE', this.url, true);
    this.headers.map((item) => {
      this.xhr.setRequestHeader(item.key, item.value);
    });
    
    this.xhr.send();
  }
}
