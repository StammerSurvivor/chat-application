import './bootstrap';
import { HttpRequestHandler } from './HttpRequestHandler';

const chatUsers = document.querySelectorAll('.chat-lists');
const welcomePageChat = document.querySelector('.welcome-page-chat');
const conversationHeaderProfile = document.querySelector('.conversation-header-profile');
const conversationHeaderUsername = document.querySelector('.conversation-header-user-name')
const hiddenReceiverId = document.querySelector('.hidden-receiver-id');
const hiddenSenderId = document.querySelector('.hidden-sender-id');
const customChat = document.querySelector('.custom-chat');
const conversationHeaderUserStatus = document.querySelector('.conversation-header-user-status');
const conversationHeaderReceiverId = document.querySelector('.conversation-header-receiver-id')
const userHiddenStatus = document.querySelectorAll('.user-hidden-status');
const csrfToken = document.querySelector(`meta[name='csrf-token']`).getAttribute('content');
const conversationInput = document.querySelector('.conversation-input');
const form = document.querySelector('#message-form');
const conversationBody = document.querySelector('.conversation-body');
const chatListMessage = document.querySelector('.chat-list-message');
const overlay = document.querySelector('.overlay');
const newChatGroup = document.querySelector('#new-chat-group');
const addGroupContactCancel = document.querySelector('.add-group-contact-cancel')
const contactsSectionList = document.querySelector('.contacts-section-list');
const image_input = document.querySelector("#pic");
const group_pic = document.querySelector(".group-pic")
const groupIcon = document.querySelector("#group-icon");
const chatIcon = document.querySelector("#chat-icon");
const groupNavigationBar = document.querySelector(".group-navigation-bar");
const chatNavigationBar = document.querySelector('.chat-navigation-bar');
const hiddenGroupId = document.querySelector('.hidden-group-id');

groupIcon.addEventListener("click", (e) => {
    chatNavigationBar.classList.add("d-none");
    groupNavigationBar.classList.remove("d-none");
});

chatIcon.addEventListener("click", (e) => {
    chatNavigationBar.classList.remove("d-none");
    groupNavigationBar.classList.add("d-none");
});
// console.log("🚀 ~ file: app.js:16 ~ csrfToken:", csrfToken);
// for (let index = 0; index < chatUsers.childElementCount; index++) {
// X-CSRF-TOKEN
//     // get id
//     console.log(chatUsers.children[index].children[0].children[1].getAttribute('user-id'));
//     console.log(chatUsers.children[index].children[0].children[2].getAttribute('data-status-color'));

// }



groupIcon.addEventListener("click", (e) => {
    console.log("🚀 ~ file: app.js:39 ~ groupIcon.addEventListener ~ e:", e.target);
});


Echo.private('user-update').listen('UserUpdated', (user) => {
    let date = getUsersDate(user.user.created_at);
    let chatList = document.createElement('div');
    chatList.className = 'chat-list chat-list';
    chatList.innerHTML = `
        <div class='chat-list-image'>
            <img src="/Images/users/${user.user.image_name}" alt='chat-list-logo'>
            <input type='hidden' user-id=${user.user.unique_id} name='user-id' />
            <input type='text' value=${user.user.active} name='user-status' class="user-status" />
        </div>
        <div class='chat-list-title'>
            <div class='chat-list-content'>
                <p class='chat-list-title-name' style='text-transform: capitalize;'>${user.user.name}</p>
                <p class='chat-list-date'>${date}</p>
            </div>
            <div class='chat-list-message'>Hello World</div>
        </div>
        `;
    chatUsers.append(chatList);
});


function getUsersDate(userDate) {
    const date = new Date(userDate);
    return date.toLocaleDateString("en-GB", {
        day: "2-digit",
        month: "2-digit",
        year: "numeric"
    });
}

function capitalize(string) {
    string = string.charAt(0).toUpperCase() + string.slice(1);
    return string;
}

chatUsers.forEach((item) => {
    console.log("🚀 ~ file: app.js:95 ~ chatUsers.forEach ~ item:", !parseInt(item.getAttribute("data-check")));

    item.addEventListener('click', (event) => {
        welcomePageChat.style.display = 'none';
        welcomePageChat.style.visibility = 'hidden';
        customChat.classList.remove('d-none');
        const closestChatList = event.target.closest('.chat-list');
        conversationHeaderProfile.children[0].setAttribute('src', closestChatList.querySelector('.chat-list-image').children[0].getAttribute('src'))
        conversationHeaderUsername.innerHTML = capitalize(closestChatList.querySelector('.chat-list-title-name').innerHTML);
        hiddenReceiverId.setAttribute('value', closestChatList.children[0].children[1].getAttribute('user-id'));

        if (!parseInt(item.getAttribute("data-check"))) {
            if (closestChatList.children[0].children[2].value == "Online") {
                conversationHeaderUserStatus.classList.remove("user-offline");
                conversationHeaderUserStatus.classList.add("user-online");
                conversationHeaderUserStatus.innerHTML = "Online";
            } else {
                conversationHeaderUserStatus.classList.remove("user-online");
                conversationHeaderUserStatus.classList.add("user-offline");
                conversationHeaderUserStatus.innerHTML = "Offline";
            }
            getUsersChat();
        } else {
            hiddenGroupId.setAttribute('value', closestChatList.children[0].children[1].value);
            getGroupChat();
        }
    });
})


function getGroupChat() {
    const request = new HttpRequestHandler();
    request.setMethod('GET');
    request.setUrl(`/get-group-chat?group_id=${hiddenGroupId.value}`);
    request.setHeader('X-CSRF-TOKEN', csrfToken);
    request.setHeader('Content-Type', 'application/json');
    request.send(function (response) {
        console.log(response);
        formattingGroupChats(response);
    });
}

function generateRandomColor() {
    // Generate random values for red, green, and blue
    let letters = '0123456789ABCDEF';
    let color = '#';

    for (let i = 0; i < 6; i++) {
        color += letters[Math.floor(Math.random() * 16)];
    }

    return color;
    // // Generate random values for the red, green, and blue components
    // var red = Math.floor(Math.random() * 128);
    // var green = Math.floor(Math.random() * 128);
    // var blue = Math.floor(Math.random() * 128);

    // // Convert the values to hexadecimal strings
    // var redHex = red.toString(16).padStart(2, '0');
    // var greenHex = green.toString(16).padStart(2, '0');
    // var blueHex = blue.toString(16).padStart(2, '0');

    // // Concatenate the hexadecimal values
    // var hexCode = '#' + redHex + greenHex + blueHex;

    return hexCode;
}


function formattingGroupChats(data) {
    conversationBody.innerHTML = '';
    conversationHeaderUserStatus.innerHTML = data.members;
    conversationHeaderUserStatus.classList.add("user-offline");

    data.pillMessages.map((item) => {
        let pillElement = document.createElement("div");
        pillElement.className = "pill group-pill text-center my-1";
        pillElement.innerHTML = `<span>${item}</span>`;
        conversationBody.append(pillElement);
    });

    data.messages.map((item, index) => {
        let message = document.createElement("div");
        if (item.sender_id === hiddenSenderId.value) {
            message.className = "outgoing-message messages";
            message.innerHTML = `
                <p>${item.message}</p>
                <div class="message-date-and-seen">
                    <span class="message-date">${getTime(item.created_at)}</span>
                    <span class="message-tick"><ion-icon name="checkmark-outline"></ion-icon></span>
                </div>`;
        } else {
            console.log(item[index]);
            message.className = "incoming-message messages";
            message.setAttribute("data-chat-time", getTime(item.created_at))
            message.innerHTML = `
            <p class="receiver-name" style="color:${generateRandomColor()}; font-weight:bolder">${capitalize(item.receiver_name)}</p>
            <p>${item.message}</p>`;
        }

        conversationBody.append(message);
    })
    conversationBody.scrollTo(0, conversationBody.scrollHeight);

}



function sendMessageSeen() {
    request.setMethod('PUT');
    request.setUrl(`/update-user-status/${user.unique_id}`);
    request.setHeader('X-CSRF-TOKEN', csrfToken);
    request.setHeader('Content-Type', 'application/json');
    request.setBody({
        active: 1
    });
    request.send((response) => {
        console.log(response);
    });
}


/***********************************************************************
*************************** STATUS UPDATE *****************************
************************************************************************/

Echo.join('user-status-update')
    .here((users) => {
        for (let i = 0; i < users.length; i++) {
            if (sender_id !== users[i].unique_id) {
                for (let index = 0; index < chatUsers.childElementCount; index++) {
                    if (chatUsers.children[index].children[0].children[1].getAttribute('user-id') == users[i].unique_id) {
                        chatUsers.children[index].children[0].children[2].value = "Online";
                    }
                }
            }
        }
    })
    .joining((user) => {
        console.log("🚀 ~ file: app.js:118 ~ .joining ~ user:", user);
        const request = new HttpRequestHandler();
        request.setMethod('PUT');
        request.setUrl(`/update-user-status/${user.unique_id}`);
        request.setHeader('X-CSRF-TOKEN', csrfToken);
        request.setHeader('Content-Type', 'application/json');
        request.setBody({
            active: 1
        });
        request.send((response) => {
            console.log(response);
        });

        for (let index = 0; index < chatUsers.childElementCount; index++) {
            if (chatUsers.children[index].children[0].children[1].getAttribute('user-id') == user.unique_id) {
                chatUsers.children[index].children[0].children[2].value = "Online";
            }
        }
    }).leaving((user) => {
        console.log("🚀 ~ file: app.js:137 ~ .leaving ~ user:", user);
        const request = new HttpRequestHandler();
        request.setMethod('PUT');
        request.setUrl(`/update-user-status/${user.unique_id}`);
        request.setHeader('X-CSRF-TOKEN', csrfToken);
        request.setHeader('Content-Type', 'application/json');
        request.setBody({
            active: 0
        });
        request.send((response) => { });
        for (let index = 0; index < chatUsers.childElementCount; index++) {
            if (chatUsers.children[index].children[0].children[1].getAttribute('user-id') == user.unique_id) {
                chatUsers.children[index].children[0].children[2].value = "Offline";
            }
        }
    })
    .listen('UserStatusEvent', (user) => {
    });


/*******************************************************************************
*************************** POST: INSERT CHATS *********************************
*********************************************************************************/

form.addEventListener('submit', (event) => {
    event.preventDefault();
    if (hiddenReceiverId.value == "null") {
        insertGroupChat();
    } else {
        insertChat();
    }
    conversationInput.children[0].value = '';
});


function insertChat() {
    const request = new HttpRequestHandler();
    request.setMethod('POST');
    request.setUrl(`/store-chat`);
    request.setHeader('X-CSRF-TOKEN', csrfToken);
    request.setHeader('Content-Type', 'application/json');
    request.setBody({
        sender_id: hiddenSenderId.value,
        receiver_id: hiddenReceiverId.value,
        message: conversationInput.children[0].value
    });
    request.send(function (response) { });
}

function insertGroupChat() {
    const request = new HttpRequestHandler();
    request.setMethod('POST');
    request.setUrl(`/store-group-chat`);
    request.setHeader('X-CSRF-TOKEN', csrfToken);
    request.setHeader('Content-Type', 'application/json');
    request.setBody({
        group_id: hiddenGroupId.value,
        sender_id: hiddenSenderId.value,
        message: conversationInput.children[0].value
    });
    request.send(function (response) {
        console.log("🚀 ~ file: app.js:268 ~ response:", response);

    });
}
// *************** GET: USER CHAT *****************

function getUsersChat() {
    const request = new HttpRequestHandler();
    request.setMethod('GET');
    request.setUrl(`/get-chats?sender_id=${hiddenSenderId.value}&receiver_id=${hiddenReceiverId.value}`);
    request.setHeader('X-CSRF-TOKEN', csrfToken);
    request.setHeader('Content-Type', 'application/json');
    request.send(function (response) {
        formattingUsersChat(response)
    });
}

// function getUsersLastMessage() {
//     const request = new HttpRequestHandler();
//     request.setMethod('GET');
//     request.setUrl(`/get-user-last-message?sender_id=${hiddenSenderId.value}&receiver_id=${hiddenReceiverId.value}`);
//     request.setHeader('X-CSRF-TOKEN', csrfToken);
//     request.setHeader('Content-Type', 'application/json');
//     request.send(function (response) {
//         console.log("🚀 ~ file: app.js:186 ~ response:", response);
//     });
// }

function getTime(dateString) {
    const date = new Date(dateString);
    const time = date.toLocaleTimeString([], { hour: 'numeric', minute: '2-digit' });
    return time;
}

function formattingUsersChat(chatData) {
    let messages = ''
    conversationBody.innerHTML = '';
    for (let index = 0; index < chatData.length; index++) {
        if (chatData[index].sender_id === hiddenSenderId.value) {
            messages += `<div class='outgoing-message messages'>
                <p>${chatData[index].message}</p>
                <div class="message-date-and-seen">
                    <span class="message-date">${getTime(chatData[index].created_at)}</span>
                    <span class="message-tick"><ion-icon name="checkmark-outline"></ion-icon></span>
                </div>
            </div>`
        } else {
            messages += `<div class='incoming-message messages' data-chat-time="${getTime(chatData[index].created_at)}"><p>${chatData[index].message}</p></div>`
        }
    }
    conversationBody.innerHTML = messages;
    conversationBody.scrollTo(0, conversationBody.scrollHeight);
}


Echo.private('broadcast-message').listen('MessageEvent', function (chatData) {
    let message = document.createElement('div')
    if (chatData.chat.sender_id === hiddenSenderId.value) {
        message.className = `outgoing-message messages`;
        message.innerHTML = `
            <p>${chatData.chat.message}</p>
            <div class="message-date-and-seen">
                <span class="message-date">${getTime(chatData.chat.created_at)}</span>
                <span class="message-tick"><ion-icon name="checkmark-outline"></ion-icon></span>
            </div>
        </div>`
    } else {
        message.className = `incoming-message messages`;
        message.setAttribute('data-chat-time', getTime(chatData.chat.created_at));
        message.innerHTML = `<p>${chatData.chat.message}</p></div>`;
    }
    chatListMessage.innerHTML = chatData.chat.message;
    conversationBody.append(message);
    conversationBody.scrollTo(0, conversationBody.scrollHeight);
});

Echo.private('broadcast-group-message').listen('GroupMessageEvent', function (groupChatData) {
    console.log("🚀 ~ file: app.js:389 ~ groupChatData:", groupChatData);

    let message = document.createElement('div')
    if (groupChatData.groupChatData.sender_id === hiddenSenderId.value) {
        message.className = `outgoing-message messages`;
        message.innerHTML = `
            <p>${groupChatData.groupChatData.message}</p>
             <div class="message-date-and-seen">
                 <span class="message-date">${getTime(groupChatData.groupChatData.created_at)}</span>
                 <span class="message-tick"><ion-icon name="checkmark-outline"></ion-icon></span>
             </div>
         </div>`;
    } else {
        message.className = `incoming-message messages`;
        message.setAttribute('data-chat-time', getTime(groupChatData.groupChatData.created_at));
        message.innerHTML = `
        <p class="receiver-name" style="color:${generateRandomColor()}; font-weight:bolder">${capitalize(groupChatData.groupChatData.name)}</p>
        <p>${groupChatData.groupChatData.message}</p>`;
    }
    chatListMessage.innerHTML = groupChatData.groupChatData.message;
    conversationBody.append(message);
    conversationBody.scrollTo(0, conversationBody.scrollHeight);
});



// GROUP MODAL
newChatGroup.addEventListener('click', function (e) {
    console.log(e.target);
    document.querySelector('.add-group-users').classList.add('active');
    overlay.classList.add('active');
    contactsSectionList.innerHTML = '';
    for (let index = 0; index < chatUsers[0].childElementCount; index++) {
        let contactsSectionLabel = document.createElement('label');
        contactsSectionLabel.className = "contacts-section-label";
        contactsSectionLabel.setAttribute("for", `checkbox-${index + 1}`);
        contactsSectionLabel.innerHTML = `
        <div class="contact-list contact-list-${index + 1}">
            <div class="contact-list-image">
                <img src="${chatUsers[0].children[index].children[0].children[0].getAttribute("src")}" alt="">
            </div>
            <div class="contact-list-name">${capitalize(chatUsers[0].children[index].children[1].children[0].children[0].innerHTML)}</div>
            <div class="contact-list-checkbox"><input type="checkbox" name="members[]" value="${chatUsers[0].children[index].children[0].children[1].getAttribute("user-id")}"
                    id="checkbox-${index + 1}"></div>
        </div>
        `;
        contactsSectionList.append(contactsSectionLabel);
    }

    // <input type='hidden' user-id=${chatUsers.children[index].children[0].children[1].getAttribute("user-id")} name='user-id' />
})

addGroupContactCancel.addEventListener('click', function (e) {
    document.querySelector('.add-group-users').classList.remove('active');
    overlay.classList.remove('active');
})




// GROUP IMAGE
image_input.addEventListener("change", function () {
    changeImage(this);
});

function changeImage(input) {
    var reader;

    if (input.files && input.files[0]) {
        reader = new FileReader();
        reader.onload = function (e) {
            group_pic.children[0].setAttribute('src', e.target.result);
        }
        reader.readAsDataURL(input.files[0]);
    }
}


