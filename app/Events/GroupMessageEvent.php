<?php

namespace App\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class GroupMessageEvent implements ShouldBroadcast {
    use Dispatchable, InteractsWithSockets, SerializesModels;
    private $groupChatData;
    /**
     * Create a new event instance.
     */
    public function __construct($groupChatData) {
        $this->groupChatData = $groupChatData;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return array<int, \Illuminate\Broadcasting\Channel>
     */
    public function broadcastOn(): array {
        return [
            new PrivateChannel('broadcast-group-message'),
        ];
    }

    public function broadcastWith() {
        return [
            'groupChatData' => $this->groupChatData
        ];
    }
}
