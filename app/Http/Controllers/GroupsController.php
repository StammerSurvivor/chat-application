<?php

namespace App\Http\Controllers;

use App\Models\Group;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;

class GroupsController extends Controller
{
    public function store(Request $request) {
        $data["unique_id"] =  Str::random(20);
        if (!empty($request->only('pic'))) {
            $data["image_name"] = $data["unique_id"] . "." . $request->file('pic')->getClientOriginalExtension();
            $request->file('pic')->move("Images/Group", $data["image_name"]);
        } else {
            $data["image_name"] = "default.png";
        }
        $data['name'] = $request->only('group-section-name')['group-section-name'];
        $data['creator_id'] = Auth::user()->unique_id;


        $group = Group::create($data);

        $membersData['group_id'] = $data["unique_id"];
        $membersData["members_id"] = $request->only('members')['members'];
        $membersData["members_id"][] = Auth::user()->unique_id;

        return redirect()->route('create-chat-group-members')->with('data', $membersData);        
    }
}
