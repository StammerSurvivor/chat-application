<?php

namespace App\Http\Controllers;

use App\Models\GroupMember;
use Illuminate\Http\Request;

class GroupMembersController extends Controller
{
    public function index(Request $request) {
        $membersData = session('data');
        $groupId = $membersData['group_id'];
        $members = $membersData['members_id'];
        foreach ($members as $member) {
            GroupMember::create([
                "group_id" => $groupId,
                "member_id" => $member
            ]);
        }

        return redirect()->route('auth.user.dashboard');
    }
}
