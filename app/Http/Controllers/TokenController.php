<?php

namespace App\Http\Controllers;

use App\Models\Token;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Str;

class TokenController extends Controller {
    protected $types = [
        'REMEMBER_ME' => 0,
        'FORGOT_PASSWORD' => 1,
        'VERIFY_EMAIL' => 2,
    ];

    public function getValidRememberMeToken($userId) {
        return $this->getValidExistingToken($userId, $this->types['REMEMBER_ME']);
    }

    public function getValidForgotPasswordToken($userId) {
        return $this->getValidExistingToken($userId, $this->types['FORGOT_PASSWORD']);
    }

    public function getValidVerifyEmailToken($userId) {
        return $this->getValidExistingToken($userId, $this->types['VERIFY_EMAIL']);
    }


    private function getValidExistingToken(int $userId, int $type) {
        if (!in_array($type, array_values($this->types))) {
            return null;
        }

        $token = Token::where([
            ['user_id', $userId],
            ['type', $type],
            ['expires_at', '>=', now()]
        ])->first();

        return $token;
    }

    private function createToken($userId, $type, $expiryTime) {
        $token = $this->getValidExistingToken($userId, $type);

        if($token) {
            return $token;
        }

        return Token::create([
            'token' => Str::random(40),
            'user_id' => $userId,
            'type' => $type,
            'expires_at' => $expiryTime,
        ]);
    }


    public function createRememberMeToken(int $userId) {
        return $this->createToken($userId, $this->types['REMEMBER_ME'], now()->addMinutes(30));
    }

    public function createForgotPasswordToken(int $userId) {
        return $this->createToken($userId, $this->types['FORGOT_PASSWORD'], now()->addMinutes(10));
    }

    public function createEmailVerifyToken(int $userId) {
        return $this->createToken($userId, $this->types['VERIFY_EMAIL'], now()->addMinutes(10));
    }


    public function deleteRememberMeToken(int $userId) {
        return $this->deleteToken($userId, $this->types['REMEMBER_ME']);
    }

    public function deleteForgotPasswordToken(int $userId) {
        return $this->deleteToken($userId, $this->types['FORGOT_PASSWORD']);
    }

    public function deleteVerifyEmailToken(int $userId) {
        return $this->deleteToken($userId, $this->types['VERIFY_EMAIL']);
    }

    public function deleteToken(int $userId, int $type) {
        $token = Token::where([
            ['user_id', $userId],
            ['type', $type]
        ])->find();

        $token->delete();
    }
}
