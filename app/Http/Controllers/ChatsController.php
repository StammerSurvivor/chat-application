<?php

namespace App\Http\Controllers;

use App\Events\MessageEvent;
use App\Models\Chat;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;

class ChatsController extends Controller {
    public function index(Request $request) {
        $senderId = $request->query->get('sender_id');
        $receiverId = $request->query->get('receiver_id');

        $result = Chat::where('sender_id', "=", $senderId)
            ->where("receiver_id", "=", $receiverId)
            ->orWhere('sender_id', "=", $receiverId)
            ->where('receiver_id', "=", $senderId)
            ->get();
        return response()->json($result);
    }


    public function store(Request $request) {
        $result = Chat::create($request->all());
        event(new MessageEvent($result));
        return response()->json($result);
    }

    public function getLastMessage(Request $request) {
        $senderId = $request->query->get('sender_id');
        $receiverId = $request->query->get('receiver_id');
        $result = Chat::where('sender_id', "=", $senderId)
            ->where("receiver_id", "=", $receiverId)
            ->orWhere('sender_id', "=", $receiverId)
            ->where('receiver_id', "=", $senderId)
            ->orderBy('created_at', 'DESC')
            ->first("message");
        return response()->json($result);
    }
}
