<?php

namespace App\Http\Controllers;

use App\Events\GroupMessageEvent;
use App\Models\GroupChat;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;

class GroupChatsController extends Controller {
    public function store(Request $request) {
        $data = array();
        $data["unique_id"] = Str::random(20);
        $data["message"] = $request->get("message");
        $data["group_id"] = $request->get("group_id");
        $data["sender_id"] = $request->get("sender_id");

        $record = GroupChat::create($data);
        
        $data["name"] = Auth::user()->name;
        $data["created_at"] = $record->created_at;

        event(new GroupMessageEvent($data));

        return response()->json($record);
    }
}

