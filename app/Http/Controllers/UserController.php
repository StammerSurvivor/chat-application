<?php

namespace App\Http\Controllers;

use App\Events\UserStatusEvent;
use App\Events\UserUpdated;
use App\Models\Chat;
use App\Models\Group;
use App\Models\GroupChat;
use App\Models\GroupMember;
use App\Models\Token;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Str;
use Illuminate\Contracts\Cookie\Factory;
use Illuminate\Support\Facades\Log;


class UserController extends Controller {

    private TokenController $tokenController;

    private const USER_KEY = "LOGGED_IN_USER_KEY";

    public function __construct(TokenController $tokenController) {
        $this->tokenController = $tokenController;
    }

    public function login(Request $request) {
        // if (!$this->getRememberWebCookie($request)) {
        //     return redirect()->route("auth.login")->with('Session Expires', 'Your Session Has been expired please login again');
        // }

        return view('auth.login');
    }

    public function register() {
        return view('auth.register');
    }

    public function store(Request $request) {

        $validatedData = User::validateRegister($request->only('name', 'email', 'password', 'password_confirmation'));
        $validatedData["unique_id"] = Str::random(30);
        $validatedData['password'] = Hash::make($validatedData['password']);
        $validatedData["verified_email"] = 0;
        $validatedData["two_f_a"] = 0;
        if (!empty($request->only('pic'))) {
            $validatedData["image_name"] = $validatedData["unique_id"] . "." . $request->file('pic')->getClientOriginalExtension();
            $request->file('pic')->move("Images/users", $validatedData["image_name"]);
        } else {
            $validatedData["image_name"] = "default.png";
        }


        $user = User::create($validatedData);
        if ($user) {
            event(new UserUpdated($user));
            return back()->with('Success', "You're Successfully register, Now Please Login!");
        } else {
            return back()->with('Fail', "Something went wrong , please try again later!");
        }
    }
    public function check(Request $request) {
        // if ($request->has('_token')) {
        $credentials = $request->only('email', 'password');
        // if ($request->has('remember_me')) {
        $validatedData = User::validateLogin($credentials);
        // SESSION
        // i change the cookie time period in auth.php

        if ($validatedData && Auth::attempt($credentials)) {
            Session::put(self::USER_KEY, Auth::user());
            // below code work done with only with this line Auth::attempt($credentials , true)
            // COOKIE
            $token = $this->tokenController->createRememberMeToken(Auth::user()->id);
            $expirationTime = Carbon::now()->addMinutes(30)->timestamp;
            $redirectResponse = new RedirectResponse(route('auth.user.dashboard'));
            $redirectResponse->withCookie(cookie('remember_me_token', $token->token, $expirationTime));
            return $redirectResponse;
            // return redirect()->route("auth.user.dashboard");
        } else {
            return back()->with('Fail', "Incorrect Email or Password,please try again!");
        }
    }

    public function dashboard(Request $request) {
        if ($request->cookies->has('remember_me_token')) {
            // dd(Auth::user());
            $tokenUserId = Token::where("token", $request->cookies->get('remember_me_token'))->get('user_id')[0]->user_id;
            $user = User::find($tokenUserId);
            Auth::setUser($user);

            $records = User::where("id", "<>", Auth::user()->id)->get();
            $senderId = Auth::user()->unique_id;
            foreach ($records as $key => $record) {
                $receiverId = $record->unique_id;
                $message = Chat::where('sender_id', "=", $senderId)
                    ->where("receiver_id", "=", $receiverId)
                    ->orWhere('sender_id', "=", $receiverId)
                    ->where('receiver_id', "=", $senderId)
                    ->orderBy('created_at', 'DESC')
                    ->first("message")->message ?? "No Conversation Started Yet";
                $record['message'] = [$message];
            }
            $groupIds = GroupMember::where("member_id", "=", Auth::user()->unique_id)->get("group_id");
            $groups = Group::get();
            $groupData = array();
            foreach ($groupIds as $groupId) {
                foreach ($groups as $group) {
                    if ($groupId->group_id == $group->unique_id) {
                        array_push($groupData, (object)[
                            "image_name" => $group->image_name,
                            "name" => $group->name,
                            "group_id" => $group->unique_id,
                            "created_at" => $group->created_at,
                        ]);
                    }
                }
            }
            $groupData = (object) $groupData;
            // Laravel 8 Eloquent pluck() method helps us to extract certain values into 1 dimension array
            // $groupIds = GroupMember::where('member_id', '=', Auth::user()->unique_id)->pluck('group_id' , 'member_id')->toArray();
            // $groups = Group::whereIn('unique_id', $groupIds)->get();

            // $groupData = $groups->map(function ($group) {
            //     return (object) [
            //         'image_name' => $group->image_name,
            //         'name' => $group->name,
            //         'group_id' => $group->unique_id,
            //         'created_at' => $group->created_at,
            //     ];
            // });

            // $groupData = $groupData->values()->all();

            return view("welcome", compact('records', 'groupData'));
        } else {
            return redirect()->route("auth.login");
        }
    }

    public function verify() {
        return view('auth.verify');
    }

    public function verified() {
        return view('auth.verified');
    }

    public function changePassword() {
        return view('auth.change-password');
    }

    public function recoveryPassword() {
        return view('auth.recovery-password');
    }

    public function verificationCode() {
        return view('auth.verification-code');
    }


    private function getRememberWebCookie($request) {
        $allCookies = $request->cookies->all();
        foreach ($allCookies as $cookieName => $cookieValue) {
            if (strpos($cookieName, 'remember_web') === 0) {
                return $cookieValue;
            }
        }

        return false;
    }


    private function isValidRememberMeToken($token) {
        $user = User::where("remember_token", $token)->first();
        return $user;
    }

    public function updateUserStatus(Request $request, $uniqueId) {
        $result = User::where("unique_id", $uniqueId)->update(['active' => $request->active]);
        return response()->json($result);
    }

    public function getGroupChat(Request $request) {
        $groupId = $request->query->get('group_id');
        $groupMessages = GroupChat::where("group_id", "=", $groupId)->get();

        $groupCreatorId = Group::where('unique_id', "=", $groupId)->get("creator_id")[0]->creator_id;
        $groupMembers = GroupMember::where('group_id', "=", $groupId)->get("member_id");
        $groupCreatorName = User::where('unique_id', "=", $groupCreatorId)->first("name")->name;

        $pillMessage = array();
        $members = array();
        $messages = array();
        foreach ($groupMembers as $member) {
            $member = User::where("unique_id", "=", $member->member_id)->first();
            if (Auth::user()->unique_id === $groupCreatorId) {
                if (Auth::user()->name !== $member->name) {
                    array_push($pillMessage, "You added " . ucfirst($member->name));
                }
            } else if (Auth::user()->id === $member->id) {
                array_push($pillMessage, ucfirst($groupCreatorName) . " added You");
            } else if ($groupCreatorName !== $member->name) {
                array_push($pillMessage, ucfirst($groupCreatorName) . " added " . ucfirst($member->name));
            }
            array_push($members, $member->name);
        }

        foreach ($groupMessages as $message) {
            $member = User::where("unique_id", "=", $message->sender_id)->first();
            if (Auth::user()->unique_id === $message->sender_id) {
                $message = [
                    "message" => $message->message,
                    "sender_name" => "You",
                    "sender_id" => $message->sender_id,
                    "created_at" => $message->created_at
                ];
            } else {
                $message = [
                    "message" => $message->message,
                    "receiver_name" => $member->name,
                    "receiver_id" => $member->unique_id,
                    "created_at" => $message->created_at
                ];
            }
            array_push($messages , $message);
        }

        $members[array_search(Auth::user()->name, $members)] = "You";

        $members = array_map('ucfirst', $members);
        $data = [
            "pillMessages" => $pillMessage,
            "members" => $members,
            "messages" => $messages
        ];

        return response()->json($data);
    }
}
