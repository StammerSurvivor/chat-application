<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class GroupChat extends Model {
    use HasFactory;

    protected $fillable = [
        "unique_id",
        "message",
        "group_id",
        "sender_id"
    ];
}
