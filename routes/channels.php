<?php

use Illuminate\Support\Facades\Broadcast;

/*
|--------------------------------------------------------------------------
| Broadcast Channels
|--------------------------------------------------------------------------
|
| Here you may register all of the event broadcasting channels that your
| application supports. The given channel authorization callbacks are
| used to check if an authenticated user can listen to the channel.
|
*/

Broadcast::channel('App.Models.User.{id}', function ($user, $id) {
    return (int) $user->id === (int) $id;
});

Broadcast::channel('user-update', function ($user) {
    // DO THE AUTHENTICATION WORK
    return true;
});

Broadcast::channel('user-status-update', function ($user) {
    // DO THE AUTHENTICATION WORK
    return $user;
});

Broadcast::channel('broadcast-message', function ($user) {
    // DO THE AUTHENTICATION WORK
    return $user;
});

Broadcast::channel('message-seen.{receiver_id}', function ($user) {
    // DO THE AUTHENTICATION WORK
    return $user;
});

Broadcast::channel('broadcast-group-message', function ($user) {
    return $user;
});
