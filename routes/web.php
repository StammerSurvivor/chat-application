<?php

use App\Http\Controllers\ChatsController;
use App\Http\Controllers\GroupChatsController;
use App\Http\Controllers\GroupMembersController;
use App\Http\Controllers\GroupsController;
use App\Http\Controllers\TokenController;
use App\Http\Controllers\UserController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

$token = new TokenController();
$user = new UserController($token);



Route::get('/', [UserController::class, 'dashboard'])->name('auth.user.dashboard');
Route::get('/auth/login', [UserController::class, 'login'])->name('auth.login');
Route::get('/auth/register', [UserController::class, 'register'])->name('auth.register');
Route::post('/auth/store', [UserController::class, 'store'])->name('auth.register.store');
Route::get('/auth/secure', [UserController::class, 'secure'])->name('auth.secure');
Route::post('/auth/check', [UserController::class, 'check'])->name('auth.login.check');


// APPLY MIDDLEWARE
Route::get('/verify', [UserController::class, 'verify'])->name('auth.verify');
Route::get('/verified', [UserController::class, 'verified'])->name('auth.verified');
Route::get('/change_password', [UserController::class, 'changePassword'])->name('auth.change.password');
Route::get('/recovery_password', [UserController::class, 'recoveryPassword'])->name('auth.recovery.password');
Route::get('/verification_code', [UserController::class, 'verificationCode'])->name('auth.verification.code');


// UPDATE
// Route::put('/update_user/{id}', [UserController::class, 'updateUser'])->name('auth.update.user');
Route::put('/update-user-status/{uniqueId}', [UserController::class, 'updateUserStatus']);


Route::get('/get-chats', [ChatsController::class, 'index']);
Route::post('/store-chat', [ChatsController::class, 'store']);
Route::get('/get-user-last-message', [ChatsController::class, 'getLastMessage']);


Route::post('/create-group', [GroupsController::class, 'store'])->name("create-chat-group");
Route::get('/create-group-members', [GroupMembersController::class, 'index'])->name("create-chat-group-members");


Route::get("/get-group-chat", [UserController::class, 'getGroupChat'])->name("get.group.chat");
Route::post("/store-group-chat", [GroupChatsController::class, 'store'])->name("store.group.chat");
