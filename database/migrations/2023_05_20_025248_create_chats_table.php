<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('chats', function (Blueprint $table) {
            $table->id();
            $table->longText('message');
            $table->string('sender_id');
            $table->string('receiver_id');
            $table->timestamps();
            
            // unique_id must be unique
            $table->foreign('sender_id')
                ->references('unique_id')
                ->on('users')
                ->onDelete('cascade');

            $table->foreign('receiver_id')
                ->references('unique_id')
                ->on('users')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('chats');
    }
};
