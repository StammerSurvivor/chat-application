<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('group_chats', function (Blueprint $table) {
            $table->id();
            $table->longText("message");
            $table->string("group_id");
            $table->string("sender_id");
            $table->timestamps();

            $table->foreign('group_id')
                ->references('unique_id')
                ->on('groups')
                ->onDelete('cascade');

            $table->foreign('sender_id')
                ->references('unique_id')
                ->on('users')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('group_chats');
    }
};
